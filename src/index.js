const express = require("express");
const connectDB = require("./db/mongoose");
const bgmRouter = require("./router/bgm");
const inviCodeRouter = require("./router/invicode");
// const imgSizeRouter = require("./router/imgsize");
const boothsRouter = require("./router/booths");
const titleRouter = require("./router/titleAnddesc");

const cors = require("cors");

// Connect to database
connectDB();

const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());
app.use(bgmRouter);
app.use(inviCodeRouter);
// app.use(imgSizeRouter);
app.use(boothsRouter);
app.use(titleRouter);

const path = require("path");
app.use(express.static(path.join(__dirname, "../minihome/build")));
app.get("/minihome/:code", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname, "../minihome/build/index.html"));
});
// const root = require('path').join(__dirname, '..', 'minihome', 'build');
// app.use("/minihome/:code", express.static(root));
// app.get("*", (req,res)=>{
//     res.sendFile('index.html', { root});
// })
// const router = new express.Router();
// router.get('/minihome/:code', (req,res)=>{
//     // console.log("???");
//     res.send("???");
//     // res.send(express.static(path.join(__dirname, '../minihome/build/index.html')));

app.listen(port, () => {
    console.log("Server is up on port " + port);
});
