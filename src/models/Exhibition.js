const mongoose = require("mongoose");

const exhibitionschema = new mongoose.Schema({
    code: {
        type: String,
        required: true,
        unique: true,
    },
    bgm: {
        type: Boolean,
        default: false,
    },
    bgmNo: {
        type: Number,
        default: 0,
    },
    invitation: {
        type: Boolean,
        default: false,
    },
    inviCode: {
        type: String,
        default: "none",
    },
    title: {
        type: String,
        // title 받으면 좋을까?
        default: "",
    },
    description: {
        type: String,
        maxlength: [2000, "description cannot be more than 1000 characters"],
        default: "",
    },
    booths: [
        {
            width: { type: Number, default: 0 },
            height: { type: Number, default: 0 },
            artist_name: {
                type: String,
                default: "",
                maxlength: [
                    100,
                    "artist name cannot be more than 100 characters",
                ],
            },
            artist_detail: {
                type: String,
                default: "",
                maxlength: [
                    1000,
                    "artist detail cannot be more than 1000 characters",
                ],
            },
            piece_name: {
                type: String,
                default: "",
                maxlength: [
                    100,
                    "piece name cannot be more than 100 characters",
                ],
            },
            piece_detail: {
                type: String,
                default: "",
                maxlength: [
                    1000,
                    "piece detail cannot be more than 1000 characters",
                ],
            },
        },
    ],
});

const Exhibition = mongoose.model("Exhibition", exhibitionschema);

module.exports = Exhibition;
