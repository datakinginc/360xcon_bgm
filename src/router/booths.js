const express = require("express");
const Exhibition = require("../models/Exhibition");

const router = new express.Router();

router.get("/imgsize/:code", async (req, res) => {
    const { code } = req.params;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({ code });
        }
    } catch (error) {
        console.log(error);
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        if (!exhibition.booths || exhibition.booths.length === 0) {
            console.log("no booths");
            exhibition.booths = [];
            const booth = {
                width: 0,
                height: 0,
                artist_name: " ",
                artist_detail: " ",
                piece_name: " ",
                piece_detail: " ",
            };
            for (let i = 0; i < 22; i++) {
                exhibition.booths.push({ booth });
            }
            await exhibition.save();
        }

        const booths = exhibition.booths;

        return res.status(200).json({ booths });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

router.post("/imgsize/:code", async (req, res) => {
    const { code } = req.params;
    const { width, height, index } = req.body;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({ code });
        }
    } catch (error) {
        console.log(error);
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        if (!exhibition.booths || exhibition.booths.length === 0) {
            console.log("no booths");
            exhibition.booths = [];
            const booth = {
                width: 0,
                height: 0,
                artist_name: " ",
                artist_detail: " ",
                piece_name: " ",
                piece_detail: " ",
            };
            for (let i = 0; i < 22; i++) {
                exhibition.booths.push({ booth });
            }
            await exhibition.save();
        }

        exhibition.booths[index].width = width;
        exhibition.booths[index].height = height;

        const response = { width, height, index };

        await exhibition.save();

        return res.status(200).json({ response });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

router.get("/artist/:code", async (req, res) => {
    const { code } = req.params;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({ code });
        }
    } catch (error) {
        console.log(error);
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        if (!exhibition.booths || exhibition.booths.length === 0) {
            console.log("no booths");
            exhibition.booths = [];
            const booth = {
                width: 0,
                height: 0,
                artist_name: " ",
                artist_detail: " ",
                piece_name: " ",
                piece_detail: " ",
            };
            for (let i = 0; i < 22; i++) {
                exhibition.booths.push({ booth });
            }
            await exhibition.save();
        }

        const booths = exhibition.booths;

        return res.status(200).json({ booths });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

router.post("/artist/:code", async (req, res) => {
    const { code } = req.params;
    const { artist_name, artist_detail, index } = req.body;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({ code });
        }
    } catch (error) {
        console.log(error);
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        if (!exhibition.booths || exhibition.booths.length === 0) {
            console.log("no booths");
            exhibition.booths = [];
            const booth = {
                width: 0,
                height: 0,
                artist_name: " ",
                artist_detail: " ",
                piece_name: " ",
                piece_detail: " ",
            };
            for (let i = 0; i < 22; i++) {
                exhibition.booths.push({ booth });
            }
            await exhibition.save();
        }

        exhibition.booths[index].artist_name = artist_name;
        exhibition.booths[index].artist_detail = artist_detail;

        const response = { artist_name, artist_detail, index };

        await exhibition.save();

        return res.status(200).json({ response });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

router.get("/piece/:code", async (req, res) => {
    const { code } = req.params;
    let exhibition = await Exhibition.findOne({ code });
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({ code });
        }
    } catch (error) {
        console.log(error);
    }
    if (exhibition) {
        if (!exhibition.booths || exhibition.booths.length === 0) {
            console.log("no booths");
            exhibition.booths = [];
            const booth = {
                width: 0,
                height: 0,
                artist_name: " ",
                artist_detail: " ",
                piece_name: " ",
                piece_detail: " ",
            };
            for (let i = 0; i < 22; i++) {
                exhibition.booths.push({ booth });
            }
            await exhibition.save();
        }

        const booths = exhibition.booths;

        return res.status(200).json({ booths });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

router.post("/piece/:code", async (req, res) => {
    const { code } = req.params;
    const { piece_name, piece_detail, index } = req.body;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({ code });
        }
    } catch (error) {
        console.log(error);
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        if (!exhibition.booths || exhibition.booths.length === 0) {
            console.log("no booths");
            exhibition.booths = [];
            const booth = {
                width: 0,
                height: 0,
                artist_name: " ",
                artist_detail: " ",
                piece_name: " ",
                piece_detail: " ",
            };
            for (let i = 0; i < 22; i++) {
                exhibition.booths.push({ booth });
            }
            await exhibition.save();
        }

        exhibition.booths[index].piece_name = piece_name;
        exhibition.booths[index].piece_detail = piece_detail;

        const response = { piece_name, piece_detail, index };

        await exhibition.save();

        return res.status(200).json({ response });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

router.get("/description/:code", async (req, res) => {
    const { code } = req.params;
    let exhibition = await Exhibition.findOne({ code });

    if (exhibition) {
        const booths = exhibition.booths;

        return res.status(200).json({ booths });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

module.exports = router;
