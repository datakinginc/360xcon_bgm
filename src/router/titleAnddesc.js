const express = require("express");
const Exhibition = require("../models/Exhibition");

const router = new express.Router();

router.get("/titleanddesc/:code", async (req, res) => {
    const { code } = req.params;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {

        const title = exhibition.title;
        const description = exhibition.description

        return res.status(200).json({ title, description });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

router.post("/titleanddesc/:code", async (req, res) => {
    const { code } = req.params;
    const { title, description } = req.body;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        exhibition.title = title;
        exhibition.description = description;

        const response = { title, description };

        await exhibition.save();

        return res.status(200).json({ response });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

module.exports = router;
