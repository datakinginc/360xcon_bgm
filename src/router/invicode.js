const express = require("express");
const Exhibition = require("../models/Exhibition");

const router = new express.Router();

router.get("/invicode/:code", async (req, res) => {
    const { code } = req.params;
    const { inviCode } = req.query;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        if (exhibition.inviCode === inviCode) {
            return res.status(200).json({ success: true });
        } else {
            return res.status(400).send({ error: "wrong invitation code" });
        }
    } else {
        return res.status(200).json({ success: true });
    }
});

router.get("/invicode/checkinvitation/:code", async (req, res) => {
    const { code } = req.params;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    let exhibition = await Exhibition.findOne({ code });

    if (exhibition) {
        const invitation = exhibition.invitation;
        return res.status(200).json({ invitation });
    } else {
        return res.status(200).json({ invitation: false });
    }
});

router.post("/invicode", async (req, res) => {
    const { code, invitation = false, inviCode = "none" } = req.body;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    let exhibition = await Exhibition.findOne({ code });

    if (exhibition) {
        exhibition.inviCode = inviCode;
        exhibition.invitation = invitation;
    } else {
        exhibition = new Exhibition({
            code,
            invitation,
            inviCode,
        });
    }

    try {
        await exhibition.save();
        res.status(201).send("invitation code 등록(수정) 완료");
    } catch (e) {
        res.status(400).send();
    }
});

module.exports = router;
