const express = require("express");
const Exhibition = require("../models/Exhibition");
const router = new express.Router();

router.post("/bgm", async (req, res) => {
    let { code, bgm, bgmNo } = req.body;
    bgm = Boolean(bgm);
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    let exhibition = await Exhibition.findOne({ code });

    if (exhibition) {
        exhibition.code = code;
        exhibition.bgm = bgm;
        exhibition.bgmNo = bgmNo;
    } else {
        exhibition = new Exhibition({
            code,
            bgm,
            bgmNo,
        });
    }

    try {
        await exhibition.save();
        res.status(201).send("bgm 정보 등록 완료");
    } catch (e) {
        res.status(400).send();
    }
});

router.get("/bgm/:code", async (req, res) => {
    const code = req.params.code;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    try {
        const exhibition = await Exhibition.findOne({ code });
        res.send(exhibition);
    } catch (e) {
        res.status(500).send();
    }
});

module.exports = router;
