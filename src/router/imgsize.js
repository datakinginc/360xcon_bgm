const express = require("express");
const Exhibition = require("../models/Exhibition");

const router = new express.Router();

router.get("/imgsize/:code", async (req, res) => {
    const { code } = req.params;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        if (!exhibition.booths || exhibition.booths.length === 0) {
            console.log("no booths");
            exhibition.booths = [];
            const booth = { width: 0, height: 0 };
            for (let i = 0; i < 22; i++) {
                exhibition.booths.push({ booth });
            }
            await exhibition.save();
        }

        const booths = exhibition.booths;

        return res.status(200).json({ booths });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

router.post("/imgsize/:code", async (req, res) => {
    const { code } = req.params;
    const { width, height, index } = req.body;
    try {
        let tmp = await Exhibition.findOne({ code });
        if (!tmp) {
            await Exhibition.create({code})
        }
    } catch (error) {
        console.log(error)
    }
    let exhibition = await Exhibition.findOne({ code });
    if (exhibition) {
        if (!exhibition.booths || exhibition.booths.length === 0) {
            console.log("no booths");
            exhibition.booths = [];
            const booth = { width: 0, height: 0 };
            for (let i = 0; i < 22; i++) {
                exhibition.booths.push({ booth });
            }
            await exhibition.save();
        }

        exhibition.booths[index] = {
            width,
            height,
        };

        const response = { ...exhibition.booths[index], index };

        await exhibition.save();

        return res.status(200).json({ response });
    } else {
        return res.status(300).json("wrong exhibition code");
    }
});

module.exports = router;
