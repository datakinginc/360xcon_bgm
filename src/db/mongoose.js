const mongoose = require("mongoose");

const connectDB = async () => {
    const conn = await mongoose.connect(
        // "mongodb+srv://dataking:xED6V5zkc1Agpt3i@cluster0.chqv8.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
        "mongodb+srv://dataking:xED6V5zkc1Agpt3i@cluster0.chqv8.mongodb.net/myThirdDatabase?retryWrites=true&w=majority",
        {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true,
        }
    );
    console.log(`MongoDB Connected: ${conn.connection.host}`);
};

module.exports = connectDB;
