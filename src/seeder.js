const fs = require("fs");
const mongoose = require("mongoose");
// Load models
const Exhibition = require("./models/Exhibition");

// Connect to DB
mongoose.connect(
    "mongodb+srv://dataking:xED6V5zkc1Agpt3i@cluster0.chqv8.mongodb.net/myThirdDatabase?retryWrites=true&w=majority",
    // "mongodb+srv://dataking:xED6V5zkc1Agpt3i@cluster0.chqv8.mongodb.net/mySecondDatabase?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useCreateIndex: true,
        useFindAndModify: false,
        useUnifiedTopology: true,
    }
);

// Read JSON files
const Exhibitions = JSON.parse(
    fs.readFileSync(`${__dirname}/../_data/exhibitions.json`, "utf-8")
);

// Import into DB
const importData = async () => {
    try {
        await Exhibition.create(Exhibitions);
        console.log("Data Imported...");
        process.exit();
    } catch (err) {
        console.error(err);
    }
};

// Delete data
const deleteData = async () => {
    try {
        await Exhibition.deleteMany();
        console.log("Data Deleted...");
        process.exit();
    } catch (err) {
        console.error(err);
    }
};

if (process.argv[2] === "-i") {
    importData();
} else if (process.argv[2] === "-d") {
    deleteData();
}
