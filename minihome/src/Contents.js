import React,{useEffect, useState} from 'react'
import axios from 'axios'

const Contents = ({url}) => {


  
  console.log(url);
  const code = url.split("/")[4];

  const [title,setTitle] = useState("");
  const [description,setDescription] = useState("");
  
  
  
  const getContent = async() => {
    
    const res = await axios.get(`https://api.dtype.360xcon.com/mobile/exhibition/dataking3/intro`);
    const data = await res.data;
    const {title} = data;
    const {description} = data;

    
    setTitle(title);
    setDescription(description);
  }
  // getContent();
  
  const [flag, setFlag] = useState(true);
  const [authors,setAuthors] = useState([]);
  const [fewauthors, setFewauthors] = useState([]);


    // getAuthor();

    useEffect(()=> {
      getContent();
    }, [])

  

    return (
            <>        
            <div className="content-header">
          <span className="content-title">{title ? title : 'Vincent van gogh'}</span>
        </div>
        <div className="content-desc">
          <span style={{fontSize:'1.8rem', marginBottom:'1.5rem', color:'#282828', fontWeight:'900'}}>전시소개</span>
          <br/>
          <br/>
        {description ? description : '등록된 전시소개가 없습니다.'}
        </div>
      
        </>
        )
}

export default React.memo(Contents);