import React,{useState} from 'react';
import {Link} from 'react-scroll'


const Navbar = () => {
    const [exhibit,setExhibit] = useState(false);
    const [author,setAuthor] = useState(false);
    const [gallery,setGallery] = useState(false);
  
    const hoverEffect = (e) => {
      // setHover(!hover);
      const type = e.currentTarget.innerText;
      if(type === "전시소개")
        setExhibit(!exhibit);
      if(type === "작가")
      setAuthor(!author);
      if(type === "갤러리")
      setGallery(!gallery);
    }
  
    return (
        <>
         <ul className="navbar">
              <Link to="1" spy={true} smooth={true}>
              <li 
                  onPointerEnter={hoverEffect} onPointerOut={hoverEffect} 
                  className={exhibit ? "onit navbar-list" : "navbar-list"} >
                  전시소개
              </li>
              </Link>
              <Link to="2" spy={true} smooth={true}>
              <li 
                  onPointerEnter={hoverEffect} onPointerOut={hoverEffect} 
                  className={author ? "onit navbar-list" : "navbar-list"} >
                  작가
              </li>
              </Link>
              <Link to="3" spy={true} smooth={true}>
              <li 
                  onPointerEnter={hoverEffect} onPointerOut={hoverEffect} 
                  className={gallery ? "onit navbar-list last-list" : "navbar-list last-list"} >
                  갤러리
              </li>
              </Link>
           </ul>
        </>
    )
}

export default React.memo(Navbar);