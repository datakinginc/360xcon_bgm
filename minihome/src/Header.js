import React, {useState} from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

const Header = ({url}) => {

    // console.log(url);
    
    const path = url.split("/")[4];
    console.log(path); // dataking
    const [headTitle,setHeadTitle] = useState("");
    const [backgroundimg, setBackgroundimg] = useState('');

    const getContent = async() => {
        const res = await axios.get(`https://api.dtype.360xcon.com/mobile/exhibition/dataking3/intro`);
        const data = res.data;
        const {title,thumbnailUrl} = data;
        setHeadTitle(title);
        setBackgroundimg(thumbnailUrl);
        console.log(data);  
      }
      getContent();


      const useStyles = makeStyles((theme) => ({
        modal: {
          display: 'flex',
          padding: theme.spacing(1),
          alignItems: 'center',
          justifyContent: 'center',
          zIndex:'9999'
          
        },
        paper: {
          backgroundColor: theme.palette.background.paper,
          border: '2px solid #000',
          boxShadow: theme.shadows[3],
          padding: theme.spacing(2, 4, 3),
        },
      }));
    
      const classes = useStyles();
        const [open, setOpen] = useState(false);

        const handleOpen = (e) => {
            setOpen(true);
            console.log(e.currentTarget);
        };

        const handleClose = () => {
            setOpen(false);
        };



      const Showmodal = (image) => {
          
        return(
        <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
      >
        <Fade in={open}>
          <div className={classes.paper} 
           style={{width: '150rem', height:'90rem', 'marginTop':'2rem'}}
          >
            {/* <iframe src={`https://360xcon.com/exhibition/${path}?autoplay=1`} width="100%" height="100%"> */}
            <iframe src={`https://dtype.360xcon.com/exhibition/${path}`} width="100%" height="100%">
            </iframe> 
          </div>
        </Fade>
      </Modal>
 )
}
    

    return (
        <>
        <div className="header-container">
       <div className="video-container">
            <img src={`${backgroundimg}`} />
       </div>
       <div className="head-title">{headTitle} Gallery</div>
       <div className="unity-play">
        <i onClick={handleOpen} class="fas fa-play-circle play-btn"></i>
       </div>
       </div>
       <Showmodal />
        </>
    )
}

export default React.memo(Header);