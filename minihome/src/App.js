import React,{useState, useRef,useEffect} from 'react';
import './App.css';
import { useLocation, useHistory} from 'react-router'
import {Link} from 'react-scroll'
import axios from 'axios';
import Header from './Header';
import Gallery from './Gallery';
import Contents from './Contents';
import Thumbnail from './Thumbnail';
import Navbar from './Navbar';

function App() {

  // const url = window.location.href;  // 아래는 예시고 정상작동할시 활성화 및 아래 주석
  //   console.log(url);
  
  // const url = "https://360xcon.com/minihome/DTYPE";
  const url = "https://360xcon.com/minihome/dataking3";
  

  const goUp = (e) => {
      window.scroll({
        top:0,
        left:0,
        behavior:"smooth"
      })
  };

 
    return(
      <>
        <Navbar />
        <Header  url={url}/>
        <div id="1" className="content-container">
        <Contents url={url} />
        <Thumbnail url={url} />
        <Gallery url={url} />
        </div>
        <div className="upBtn" onClick={goUp}>
        <i class="fas fa-long-arrow-alt-up"></i>
        </div>
      </>
    )
  
}

export default App;
