import axios from 'axios';
import React, {
  useState,
  useEffect,
  useRef
} from 'react';
import {
  useParams
} from 'react-router';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Fade from '@material-ui/core/Fade';



const Gallery = ({url}) => {
  
  const [images, setImages] = useState([]);
  const [currentImage, setCurrentImage] = useState({});
  const code = url.split("/")[4];


  const loadBooths = async () => {
    const res = await axios.get(`https://api.dtype.360xcon.com/internal/exhibition/${code}`, 
    {
      headers: {
          "ex-internal-key": "clqVoEuY3EI7RMiKIOqjKAM7tmoNKApZ",
      },
  }
    );
    // const data = res.json();
    
    
    // console.log('booths');
    // console.log(res);
    // console.log(res.data.booths);

    const booths = res.data.booths;

    const tempBooths = []

      for (var i = 0; i < booths.length; i++) {
        tempBooths.push(booths[i]);
      }

    setImages(tempBooths);
  }
  
  useEffect(()=> {
    // getBooths();
    loadBooths();
    
  }, [])
  // getBooths();
  
  console.log('imagesssssssssssss');
  console.log(images)

  const useStyles = makeStyles((theme) => ({
    modal: {
      display: 'flex',
      padding: theme.spacing(1),
      alignItems: 'center',
      justifyContent: 'center',
      zIndex:'9999',
      position:'relative',
      border:'none', 
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[3],
      padding: theme.spacing(2, 4, 3),
      position:'relative',
      display:'flex',
      justifyContent:'center',
    },
    modal2: {
      display: 'flex',
      padding: theme.spacing(1),
      alignItems: 'center',
      justifyContent: 'center',
      zIndex:'9999',
      position:'relative',
      border:'none',     
      backgroundColor:'black',
    },
  }));


  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [open2, setOpen2] = useState(false);
  const [currentInform, setCurrentInform] = useState({});


  useEffect(()=> {
    // console.log('current image');
    // console.log(currentImage);
    // console.log(currentImage.informations);
    // const inform = JSON.stringify(currentImage.informations);
    // console.log("ggg: "+ currentInform.artist_name);
    // let temp = currentImage.informations
    // console.log('temp...')
    // if(temp.artist_name) {
    //   console.log(temp.artist_name);
    // }
    

    
    // console.log(currentImage.informations['piece_name'])
  }, [currentImage])

  const handleOpen = (id) => {
    setOpen(true);
    console.log('id')
    console.log(id);
    // const src = e.currentTarget.parentNode.parentNode.firstChild.src;
    // const imgSrc = src.split("/")[4];
    // console.log(imgSrc);
      function isApple(element)  {
        if(element._id === id)  {
          return true;
        }
      }
      
      const modalbooth = images.find(isApple);
      console.log('modalbooth')
      console.log(modalbooth)
      setCurrentInform(modalbooth.informations);
      setCurrentImage(modalbooth);

      

  };
  console.log('1231312312');
  console.log(currentImage);
  const handleOpen2 = (e) => {
    setOpen2(true);
  }

  const handleClose = () => {
    setOpen(false);
    setCurrentImage({});
  };
  const handleClose2 = () => {
      setOpen2(false);
  };


  function imgError(e) {
    e.target.src="https://dataking.blob.core.windows.net/images/default_img.png";
  }

 
 const Imgmodal = () => {

    return(
      <Modal
      className={classes.modal2}
      open={open2}
      onClose={handleClose2}
      closeAfterTransition
      >
        <Fade
          in={open2}
          style={{outline:'none'}}
        >
          <div className={classes.paper2}>
            <div className="imgmodal-img"
            //  style={{
            //   backgroundImage:
            //   `url(https://xconimages.blob.core.windows.net/dtype/${currentImage.assetId})`}}
            //   onError={imgError}
            >
              <img src={`https://xconimages.blob.core.windows.net/dtype/${currentImage.assetId}`} />
              <div><i onClick={handleClose2} className="imgmodal-close-btn-icon fas fa-times"></i></div>
            </div>
            <div className="imgmodal-img-title">{currentInform.piece_name}</div>
            <div className="imgmodal-img-author">{currentInform.artist_name}</div>
          </div>
        </Fade>
      </Modal>
    )

 }

 const Showmodal = () => {

          return(
          <Modal
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          disableAutoFocus={true}
        >
          <Fade in={open}
        style={{outline:'none'}}
          >
            <div className={classes.paper}>
              <div 
              // style={{
              //       backgroundImage:
              //       `url(https://xconimages.blob.core.windows.net/dtype/${currentImage.assetId})`}}
                    className="modal-image"
                    disableAutoFocus={true}
                    // onError={imgError}
                    >
                      <img src={`https://xconimages.blob.core.windows.net/dtype/${currentImage.assetId}`} onError={imgError} />
                    <div className="imgmodal-btn">
                      <i onClick={handleOpen2} className="imgmodal-btn-icon fas fa-search-plus"></i>
                    </div>
                  </div>
                <div className="infomodal">
                    <div className="informodal-close-btn"><i onClick={handleClose} className="infomodal-close-btn-icon fas fa-times"></i></div>
                     <div className="piece-title">
                         {currentInform.piece_name}
                    </div>
                    <div className="piece-author-name">{currentInform.artist_name}</div>
                    <div className="piece-desc-header">작품 소개</div>
                    <div className="piece-desc">
                   {currentInform.piece_detail}
                    </div>
                </div>
            </div>
          </Fade>
        </Modal>
   )
 }



  return ( <>
    <div  className = "works-container" >
    <div id = "3" className = "works-title" > Gallery </div>
    <div id = "box" className = "works-box" >
       {images.map(img => {
        //  console.log('rowssssssss');
        //  console.log(img)
          return (<>
           <figure class="snip1193" >
            <img src={
            `https://xconimages.blob.core.windows.net/dtype/${img.assetId}`
            }
            onError={imgError}
            />
            <figcaption>
              <h4 className="img-header">xcon Gallery</h4>
                <span onClick={()=> handleOpen(img._id)}  className="bottom-left">
                  Click Here
                </span>
            </figcaption>
          </figure>
            </>
          )
        })
    } </div> 
    </div> 

    <Showmodal />
    <Imgmodal />
    </>
  )
}

export default React.memo(Gallery);


