import React,{useState, useEffect} from 'react';
import { useLocation } from 'react-router';
import axios from 'axios';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
const Thumbnail = ({url}) => {

    const code = url.split("/")[4];
    // console.log(path + "썸네일"); // dataking

    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows:true,
      autoplay: true
    };

    
    const [flag, setFlag] = useState(true);
    const [authors,setAuthors] = useState([]);
    const [thumbnail, setThumbnail] = useState('');

  
    const getAuthor = async () => {
      const res = await axios.get('https://api.dtype.360xcon.com/mobile/exhibition/dataking3/intro');

      const data = res.data;
      console.log('섬네일쪽 데이터 ')
      console.log(data);

      const {artistList,thumbnailUrl} = data;
      setAuthors(artistList);
      setThumbnail(thumbnailUrl);
    }

    

      useEffect(()=> {
        getAuthor();
      }, [])
  

      function imgError(e) {
        e.target.src="https://dataking.blob.core.windows.net/images/default_img.png";
      }

    return (
        <>
        <div className="thumbnail-img"
            style={{backgroundImage:`url(${thumbnail})`}}
        > 
       </div>
        <div id="2" className="artist-header">Artists</div>
        <Slider {...settings}>
          {authors.map((author)=>
           <div className="slider-box">
           <div  className="authors-box">
             <div className="authors-name">
               {author.artist_name}
             </div>
             <div className="authors-desc">
               {author.artist_detail}
             </div>
           </div>
          </div>
           )}
        </Slider>
        </>
    )
}

export default React.memo(Thumbnail);

